Changelog:

v0.3
 * Added Research Factor to staff panels
 * Added auto update checker.
 * Changed text fields to spinners.
 * Modified parser so that values populated are actual values, and parsed back to factors on write.

v0.2:
 * Fixed Data/Research Points issue
 * Added Logging to gdtedit.log

v0.1:

Initial Release. Many problems.