package com.hewhowas.gdtedit.controls;

import javax.swing.*;
import java.awt.*;
import java.security.interfaces.DSAKey;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 19/09/13
 * Time: 11:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class DatabaseSpinner extends JSpinner {
    public DatabaseSpinner(){
       getTextField().setBackground(Color.green);
    }

    JTextField getTextField(){
        return ((JSpinner.DefaultEditor)getEditor()).getTextField();
    }
}
