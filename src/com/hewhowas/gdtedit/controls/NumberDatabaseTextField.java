package com.hewhowas.gdtedit.controls;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 4/09/13
 * Time: 8:32 PM
 * Further restricts the input to numbers and periods only.
 */
public class NumberDatabaseTextField extends DatabaseTextField {
    private static Color defaultNormalColor = Color.green;
    private static Color defaultModifiedColor = Color.red;

    public NumberDatabaseTextField(Color normalColor, Color modifiedColor){
        super(normalColor, modifiedColor);
    }

    @Override
    protected void processKeyEvent(KeyEvent e) {
        if(Pattern.matches("[0-9.]+", String.valueOf(e.getKeyChar())) || e.isActionKey() ||
                e.getKeyCode() == 8 || e.getKeyCode() == 46) {
            super.processKeyEvent(e);
        }
    }

    public NumberDatabaseTextField(){
        super(defaultNormalColor, defaultModifiedColor);
    }
}
