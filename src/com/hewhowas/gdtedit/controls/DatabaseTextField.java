package com.hewhowas.gdtedit.controls;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 4/09/13
 * Time: 8:25 PM
 * This is a custom JTextField where the background colour changes on text changed, or on focus being lost. This is
 * designed to indicate a database write occuring when focus is lost.
 */
public class DatabaseTextField extends JTextField {

    public DatabaseTextField(final Color normalColor, final Color modifiedColor){
        final StringObject originalText = new StringObject();
        setBackground(normalColor);
        addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if(!getText().equals(""))
                    originalText.string = getText();
            }

            @Override
            public void focusLost(FocusEvent e) {
                setBackground(normalColor);
            }
        });
        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                setBackground(modifiedColor);
            }

            @Override
            public void keyPressed(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void keyReleased(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }

    private class StringObject {
        String string;
    }

    private class BoolObject {
        Boolean bool;
    }
}


