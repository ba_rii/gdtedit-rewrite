package com.hewhowas.gdtedit;

import com.hewhowas.commons.autoupdate.AutoUpdateException;
import com.hewhowas.commons.autoupdate.AutoUpdateResponse;
import com.hewhowas.commons.autoupdate.AutoUpdateResponseListener;
import com.hewhowas.commons.autoupdate.BasicHttpAutoUpdater;
import com.hewhowas.commons.util.FileUtil;
import com.hewhowas.gdtapi.savefile.SaveFileController;
import com.hewhowas.gdtapi.savefile.SaveFileException;
import com.hewhowas.gdtapi.savefile.StandaloneFileController;
import com.hewhowas.gdtapi.savefile.SteamFileController;
import com.hewhowas.gdtapi.savegame.SaveGame;
import com.hewhowas.gdtapi.savegame.StaffMember;
import com.hewhowas.gdtedit.controls.DatabaseSpinner;
import com.hewhowas.gdtedit.controls.DatabaseTextField;
import com.hewhowas.gdtedit.controls.NumberDatabaseTextField;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static org.apache.log4j.PropertyConfigurator.*;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 3/09/13
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class CompanyEdit extends JFrame implements AutoUpdateResponseListener{
    private JButton okButton;
    private JButton cancelButton;
    private JPanel rootPanel;
    private JPanel buttonPanel;
    private JPanel CompanyPanel;
    private JTextField txtCompanyName;
    private JTextField txtCompanyCash;
    private JTextField txtCompanyFans;
    private JLabel lblFans;
    private JLabel lblName;
    private JLabel lblCash;
    private JTextField txtData;
    private JLabel lblData;
    private JTabbedPane staffTabPane;
    private JComboBox cmbSlot;
    private JLabel lblSlot;
    private JPanel staffPanel;
    private JPanel slotPanel;

    private static final String seperator = System.getProperty("file.separator");
    private SaveFileController saveFileController;
    private HashMap<Integer, SaveGame> saveGameMap;
    private int selectedSlot;
    private SaveGame selectedGame;

    private static Logger LOGFILE = Logger.getLogger(CompanyEdit.class);

    public static void main(String[] args) {
        initLogging();
        JFrame frame = new CompanyEdit();
        LOGFILE.info(String.format("Starting GDT Edit v%s", Constants.versionString) );
        frame.pack();
        frame.setVisible(true);
        frame.setTitle("GDT Save Editor v" + Constants.versionString);
    }

    private static void initLogging(){
        Properties props = new Properties();
        try {
            props.load(new FileInputStream("log4j.properties"));
        } catch (Exception e) {
            System.out.println("Error initializing logging system");
        }
        configure(props);
    }



    public CompanyEdit(){
        super();
        initMenu();
        initUi();
        checkForUpdate();
     }

    private void initUi(){
        LOGFILE.info("Intializing UI");
        setContentPane(rootPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LOGFILE.info("Cancel button clicked, closing window.");
                CompanyEdit.this.dispose();
            }
        });
        cmbSlot.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED){
                    String item = e.getItem().toString();
                    if(item.equals("autosave")){
                        selectedSlot = 0;
                    } else {
                        selectedSlot = Integer.parseInt(item);
                    }
                    selectedGame = saveGameMap.get(selectedSlot);
                    LOGFILE.info("New saveGame selected: " + selectedGame.getSaveSlot());
                    populateCompanyPanel(selectedGame);
                    populateStaffPanel(selectedGame);
                }
            }
        });
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LOGFILE.info("Ok button clicked, beginning write.");
                try {
                    int reply = JOptionPane.showConfirmDialog(CompanyEdit.this,
                            "This will overwrite your current save games. Proceed?","Caution",JOptionPane.YES_NO_OPTION);
                    if(reply == JOptionPane.YES_OPTION){
                        try{
                            saveFileController.backupGameFile();
                            saveFileController.writeAllSaveGames(saveGameMap);
                            JOptionPane.showMessageDialog(CompanyEdit.this, "Succesfully saved games.");
                        } catch (SaveFileException e1) {
                            JOptionPane.showMessageDialog(CompanyEdit.this, String.format("Error saving game: %s",
                                    e1.getLocalizedMessage()));
                        }

                    }
                } catch (IOException e1) {
                    JOptionPane.showMessageDialog(CompanyEdit.this, "Error saving games: " + e1.getLocalizedMessage());
                    LOGFILE.error(e1);
                }
            }
        });
        txtCompanyCash.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                // nop
            }

            @Override
            public void focusLost(FocusEvent e) {
                if(selectedGame != null){
                    LOGFILE.info(String.format("Updating company cash value: %s -> %s"
                            , String.valueOf(selectedGame.getCompanyCash()), txtCompanyCash.getText()));
                    selectedGame.setCompanyCash(Long.parseLong(txtCompanyCash.getText()));
                }
            }
        });
        txtCompanyName.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                // nop
            }

            @Override
            public void focusLost(FocusEvent e) {
                if(selectedGame != null){
                    LOGFILE.info(String.format("Updating company name value: %s -> %s"
                            , selectedGame.getCompanyName(), txtCompanyName.getText()));
                    selectedGame.setCompanyName(txtCompanyName.getText());
                }
            }
        });
        txtCompanyFans.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                // nop
            }

            @Override
            public void focusLost(FocusEvent e) {
                if(selectedGame != null) {
                    LOGFILE.info(String.format("Updating company fan value: %s -> %s"
                            , String.valueOf(selectedGame.getCompanyFans()), txtCompanyFans.getText()));
                    selectedGame.setCompanyFans(Integer.parseInt(txtCompanyFans.getText()));
                }
            }
        });
        txtData.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                // nop
            }

            @Override
            public void focusLost(FocusEvent e) {
                if(selectedGame != null) {
                    LOGFILE.info(String.format("Updating company fan value: %s -> %s"
                            , String.valueOf(selectedGame.getCompanyData()), txtData.getText()));
                    selectedGame.setCompanyData(Integer.parseInt(txtData.getText()));
                }
            }
        });
    }

    private void populateUi(SaveFileController loader) throws IOException, SaveFileException {
        LOGFILE.info("Populating UI");
        saveGameMap = loader.loadAllSaveGames();
        cmbSlot.removeAllItems();
        for(Map.Entry<Integer, SaveGame> saveGameEntry : saveGameMap.entrySet()){
            if(saveGameEntry.getKey() == 0){
                cmbSlot.addItem("autosave");
            } else {
                cmbSlot.addItem(String.valueOf(saveGameEntry.getKey()));
            }
        }
        if(cmbSlot.getItemCount() >= 1){
            cmbSlot.setSelectedIndex(0);
        }
    }

    private void initMenu(){
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenuItem openSteamFolder = new JMenuItem("Open Steam Folder");
        openSteamFolder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                File defaultDir = SteamFileController.getDefaultProfilePath();
                if(defaultDir != null){
                    fileChooser.setCurrentDirectory(defaultDir);
                }
                if (fileChooser.showOpenDialog(CompanyEdit.this) == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    //Get a list of the userData directory.
                    File userData = new File(file.getAbsoluteFile() + System.getProperty("file.separator") + "userdata");
                    String[] directories = FileUtil.getSubfolders(userData.getAbsolutePath());
                    if(directories == null || directories.length == 0){
                        JOptionPane.showMessageDialog(CompanyEdit.this, String.format("Selected folder does not appear " +
                                "to be a Steam install directory. Unable to load saves"));
                        clearStaffPanel();
                        clearCompanyPanel();
                        return;
                    } else if(directories.length == 1) { //Only one profile, load the game.
                        saveFileController = new SteamFileController(new File(userData.getAbsoluteFile() + System.getProperty("file.separator") + directories[0]));
                    } else {
                        //Select Profile Folder Manually
                        JOptionPane.showMessageDialog(CompanyEdit.this, "Multiple Steam profile directories found. Please select one.");
                        fileChooser.setCurrentDirectory(userData);
                        if (fileChooser.showOpenDialog(CompanyEdit.this) == JFileChooser.APPROVE_OPTION) {
                            File dir = fileChooser.getSelectedFile();
                            saveFileController = new SteamFileController(dir);
                        }
                    }
                    if(saveFileController != null){
                        try {
                            populateUi(saveFileController);
                        } catch (IOException e1) {
                            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        } catch (SaveFileException e1) {
                            JOptionPane.showMessageDialog(CompanyEdit.this, String.format("Error loading games: %s",
                                    e1.getLocalizedMessage()));
                            clearCompanyPanel();
                            clearStaffPanel();
                        }
                    }
                }
            }
        });

        JMenuItem openSaveDatabase = new JMenuItem("Open Save Database");
        openSaveDatabase.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("GameDevTycoon Save Database", "localstorage");
                fileChooser.setFileFilter(filter);
                fileChooser.setFileHidingEnabled(false);
                String defaultDirectory = System.getProperty("user.home") + seperator + "AppData" + seperator + "Local" + seperator +
                        "Game Dev Tycoon - Steam" + seperator +"Local Storage";
                fileChooser.setCurrentDirectory( new File(defaultDirectory));
                if (fileChooser.showOpenDialog(CompanyEdit.this) == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    System.out.println(file.getAbsolutePath());
                    //Get a list of the userData directory.
                    saveFileController = new StandaloneFileController(file);
                    if(saveFileController != null){
                        try {
                            populateUi(saveFileController);
                        } catch (IOException e1) {
                            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        } catch (SaveFileException e1) {
                            JOptionPane.showMessageDialog(CompanyEdit.this, String.format("Error loading games: %s",
                                    e1.getLocalizedMessage()));
                            clearCompanyPanel();
                            clearStaffPanel();
                        }
                    }
                }
            }
        });

        fileMenu.add(openSteamFolder);
        fileMenu.add(openSaveDatabase);
        menuBar.add(fileMenu);
        setJMenuBar(menuBar);
    }

    private void createUIComponents() {
        txtCompanyName = new DatabaseTextField(Color.green, Color.red);
    }

    private void populateStaffPanel(final SaveGame saveGame){
        staffTabPane.removeAll();
        int i = 0;
        for(final Map.Entry<String, StaffMember> staffEntry : saveGame.getStaffRoster().entrySet()){
            final StaffMember staff = staffEntry.getValue();
            GridBagConstraints con = new GridBagConstraints();
            JPanel staffMemberPanel = new JPanel();
            staffMemberPanel.setName(staff.getName());
            staffMemberPanel.setLayout(new GridBagLayout());
            staffTabPane.add(staffMemberPanel, i++);
            //Common settings for constraints.
            con.gridwidth = 1;
            con.gridheight = 1;
            con.weightx = 1.0;
            con.weighty = 1.0;
            con.ipadx = 2;
            con.ipady = 2;
            con.fill = GridBagConstraints.HORIZONTAL;

            JLabel lblDesignFactor = new CustomJLabel();
            lblDesignFactor.setText("Design Factor:");
            con.gridx = 0;
            con.gridy = 0;
            con.anchor = GridBagConstraints.WEST;
            staffMemberPanel.add(lblDesignFactor, con);
            final JSpinner spinDesignFactor = new DatabaseSpinner();
            spinDesignFactor.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    staff.setDesignFactor(jSpinnerValueToInt(spinDesignFactor));
                }
            });
            spinDesignFactor.setValue(staff.getDesignFactor());
            con.gridx = 1;
            con.gridy = 0;
            con.anchor = GridBagConstraints.EAST;
            staffMemberPanel.add(spinDesignFactor, con);

            JLabel lblTechnologyFactor = new CustomJLabel();
            lblTechnologyFactor.setText("Tech Factor:");
            con.gridx = 0;
            con.gridy = 1;
            con.anchor = GridBagConstraints.WEST;
            staffMemberPanel.add(lblTechnologyFactor, con);
            final JSpinner spinTechnologyFactor = new DatabaseSpinner();
            spinTechnologyFactor.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    staff.setTechnologyFactor(jSpinnerValueToInt(spinTechnologyFactor));
                }
            });
            spinTechnologyFactor.setValue(staff.getTechnologyFactor());
            con.gridx = 1;
            con.gridy = 1;
            con.anchor = GridBagConstraints.EAST;
            staffMemberPanel.add(spinTechnologyFactor, con);

            JLabel lblSpeedFactor = new CustomJLabel();
            lblSpeedFactor.setText("Speed Factor:");
            con.gridx = 0;
            con.gridy = 2;
            con.anchor = GridBagConstraints.WEST;
            staffMemberPanel.add(lblSpeedFactor, con);
            final JSpinner spinSpeedFactor = new DatabaseSpinner();
            spinSpeedFactor.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    staff.setSpeedFactor(jSpinnerValueToInt(spinSpeedFactor));
                }
            });
            spinSpeedFactor.setValue(staff.getSpeedFactor());
            con.gridx = 1;
            con.gridy = 2;
            con.anchor = GridBagConstraints.EAST;
            staffMemberPanel.add(spinSpeedFactor, con);

            JLabel lblQualityFactor = new CustomJLabel();
            lblQualityFactor.setText("Quality Factor:");
            con.gridx = 0;
            con.gridy = 3;
            con.anchor = GridBagConstraints.WEST;
            staffMemberPanel.add(lblQualityFactor, con);
            final JSpinner spinQualityFactor = new DatabaseSpinner();
            spinQualityFactor.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    System.out.println("Change Quality: " + spinQualityFactor.getValue().toString());
                    staff.setQualityFactor(jSpinnerValueToInt(spinQualityFactor));
                }
            });
            spinQualityFactor.setValue(staff.getQualityFactor());
            con.gridx = 1;
            con.gridy = 3;
            con.anchor = GridBagConstraints.EAST;
            staffMemberPanel.add(spinQualityFactor, con);

            JLabel lblResearchFactor = new CustomJLabel();
            lblResearchFactor.setText("Research Factor:");
            con.gridx = 0;
            con.gridy = 4;
            con.anchor = GridBagConstraints.WEST;
            staffMemberPanel.add(lblResearchFactor, con);
            final JSpinner spinResearchFactor = new DatabaseSpinner();
            spinQualityFactor.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    System.out.println("Change Quality: " + spinResearchFactor.getValue().toString());
                    staff.setResearchFactor(jSpinnerValueToInt(spinResearchFactor));
                }
            });
            spinResearchFactor.setValue(staff.getResearchFactor());
            con.gridx = 1;
            con.gridy = 4;
            con.anchor = GridBagConstraints.EAST;
            staffMemberPanel.add(spinResearchFactor, con);

            JLabel lblSalary = new CustomJLabel();
            lblSalary.setText("Salary:");
            con.gridx = 0;
            con.gridy = 5;
            con.anchor = GridBagConstraints.WEST;
            staffMemberPanel.add(lblSalary, con);
            final DatabaseSpinner spinnerSalary = new DatabaseSpinner();
            spinnerSalary.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    try{
                        staff.setSalary((Long) spinnerSalary.getValue());
                    } catch (Exception e1){
                        staff.setSalary((Integer) spinnerSalary.getValue());
                    }
                }
            });
            spinnerSalary.setMinimumSize(new Dimension(150, -1));
            spinnerSalary.setValue(staff.getSalary());
            con.gridx = 1;
            con.gridy = 5;
            con.anchor = GridBagConstraints.EAST;
            staffMemberPanel.add(spinnerSalary, con);


            JLabel lblBugFixingDelta = new CustomJLabel();
            lblBugFixingDelta.setText("Bug Fixing Delta:");
            con.gridx = 0;
            con.gridy = 6;
            con.anchor = GridBagConstraints.WEST;
            staffMemberPanel.add(lblBugFixingDelta, con);
            final DatabaseSpinner spinnerBugFixingDelta = new DatabaseSpinner();
            spinnerBugFixingDelta.addChangeListener(new ChangeListener() {


                @Override
                public void stateChanged(ChangeEvent e) {
                    try{
                        staff.setBugFixingDelta((Long) spinnerBugFixingDelta.getValue());
                    } catch (Exception e1){
                        staff.setBugFixingDelta((Integer) spinnerBugFixingDelta.getValue());
                    }
                }
            });
            spinnerBugFixingDelta.setMinimumSize(new Dimension(150, -1));
            spinnerBugFixingDelta.setValue(staff.getBugFixingDelta());

            con.gridx = 1;
            con.gridy = 6;
            con.weightx = 1.0;
            con.anchor = GridBagConstraints.EAST;
            staffMemberPanel.add(spinnerBugFixingDelta, con);

            JLabel lblRelaxDelta = new CustomJLabel();
            lblRelaxDelta.setText("Relax Delta:");
            con.gridx = 0;
            con.gridy = 7;
            con.anchor = GridBagConstraints.WEST;
            staffMemberPanel.add(lblRelaxDelta, con);
            final DatabaseSpinner spinnerRelaxDelta = new DatabaseSpinner();
            spinnerSalary.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    try{
                        staff.setRelaxDelta((Long) spinnerRelaxDelta.getValue());
                    } catch (Exception e1){
                        staff.setRelaxDelta((Integer) spinnerRelaxDelta.getValue());
                    }
                }
            });
            spinnerRelaxDelta.setMinimumSize(new Dimension(150, -1));
            spinnerRelaxDelta.setValue(staff.getRelaxDelta());

            con.gridx = 1;
            con.gridy = 7;
            con.weightx = 1.0;
            con.anchor = GridBagConstraints.EAST;
            staffMemberPanel.add(spinnerRelaxDelta, con);

            JLabel lblSex = new CustomJLabel();
            lblSex.setText("Sex:");
            con.gridx = 0;
            con.gridy = 8;
            con.anchor = GridBagConstraints.WEST;
            staffMemberPanel.add(lblSex, con);
            final JComboBox cmbSex = new JComboBox(StaffMember.Sex.values());
            cmbSex.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if(e.getStateChange() == ItemEvent.SELECTED){
                        staff.setSex((StaffMember.Sex)cmbSex.getSelectedItem());
                    }
                }
            });
            cmbSex.setSelectedItem(staff.getSex());
            con.gridx = 1;
            con.gridy = 8;
            con.anchor = GridBagConstraints.EAST;
            staffMemberPanel.add(cmbSex, con);
        }
    }

    private int jSpinnerValueToInt(JSpinner spinner){
        Object spinValueObject = spinner.getValue();
        int spinValue = 0;
        try {
            spinValue = (Integer) spinValueObject;
        } catch (Exception e)
        {
            spinValue = ((Double) spinValueObject).intValue();
        }

        return spinValue;
    }

    private void clearStaffPanel(){
        staffTabPane.removeAll();
    }

    private void populateCompanyPanel(SaveGame saveGame){
        txtCompanyName.setText(saveGame.getCompanyName());
        txtCompanyCash.setText(String.valueOf(saveGame.getCompanyCash()));
        txtCompanyFans.setText(String.valueOf(saveGame.getCompanyFans()));
        txtData.setText(String.valueOf(saveGame.getCompanyData()));
    }

    private void clearCompanyPanel(){
        txtCompanyName.setText("");
        txtData.setText("");
        txtCompanyFans.setText("");
        txtCompanyCash.setText("");
    }

    private class CustomJLabel extends JLabel {
        CustomJLabel(){
            setHorizontalAlignment(LEFT);
        }
    }

    //BOF Auto Update

    private void checkForUpdate(){
        String updateURL = "http://jameshachem.com/gdtUpdate";
        BasicHttpAutoUpdater updater = new BasicHttpAutoUpdater(updateURL);
        updater.addAutoUpdateResponseListener(this);
        Thread t = new Thread(updater);
        t.run();
    }

    @Override
    public void onAutoUpdateResponseSuccess(AutoUpdateResponse response) {
        if(response.getNewVersionNo() > Constants.versionNo){
            LOGFILE.info(String.format("New update available. Current version: %s. New version: %s", Constants.versionString,
                    response.getNewVersionString()));
            int userInput = JOptionPane.showConfirmDialog(this, "A new version is available. Go to download page?", "Update Available", JOptionPane.YES_NO_OPTION);
            if(userInput == JOptionPane.YES_OPTION){
                try {
                    URI uri = new URL(response.getUpdateUrl().trim()).toURI();
                    Desktop.getDesktop().browse(uri);
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(this, "Error opening download URL: " + e.getLocalizedMessage());
                } catch (URISyntaxException e) {
                    LOGFILE.error(e);
                }
            }
        }
    }

    @Override
    public void onAutoUpdateResponseFailure(AutoUpdateException exception) {
        LOGFILE.error("Error communicating with update server.");
        LOGFILE.error(exception.getMessage());
    }

    //EOF Auto Update

}
