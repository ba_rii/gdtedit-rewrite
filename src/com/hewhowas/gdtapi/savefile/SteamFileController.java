package com.hewhowas.gdtapi.savefile;

import com.hewhowas.commons.util.FileUtil;
import com.hewhowas.commons.util.OSUtil;
import com.hewhowas.commons.util.ZipUtil;
import com.hewhowas.gdtapi.savegame.SaveGame;
import com.hewhowas.gdtapi.savegame.SaveGameParser;
import com.hewhowas.gdtapi.Constants;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 3/09/13
 * Time: 8:24 AM
 * To change this template use File | Settings | File Templates.
 */
public class SteamFileController implements SaveFileController {

    private File steamProfileFolder;
    private String gameFolderPath;

    Logger LOGFILE = Logger.getLogger(getClass());

    /**
     *
     * @param steamProfileFolder The profile folder to load saves from.
     *                           Under windows is traditionally "C:\\Program Files (x86)\\Steam\\userdata\\<ProfileNumber>\\239820\\"
     */
    public SteamFileController(File steamProfileFolder){
        LOGFILE.info("new SteamFileController created: " + steamProfileFolder.getAbsolutePath());
        this.steamProfileFolder = steamProfileFolder;
        gameFolderPath = steamProfileFolder.getAbsolutePath() + Constants.separator + "239820" + Constants.separator
                + "remote";
    }

    /**
     *
     * @return A HashMap containing all save games. Autosave is in slot 0, all others are in their respective slots.
     * @throws SteamIOException when no profile folders are found in the given steam directory. Indicates it wasn't the
     * correct one.
     * @throws IOException
     */
    @Override
    public HashMap<Integer, SaveGame> loadAllSaveGames() throws SaveFileException, IOException {
        HashMap<Integer, SaveGame> retVal = new HashMap<Integer, SaveGame>();
        File gameFolder = new File(gameFolderPath);
        if(gameFolder.exists() && gameFolder.isDirectory()){
            File slot0 = new File(gameFolder.getAbsolutePath() + Constants.separator + "slot_auto");
            File slot1 = new File(gameFolder.getAbsolutePath() + Constants.separator + "slot_1");
            File slot2 = new File(gameFolder.getAbsolutePath() + Constants.separator + "slot_2");
            File slot3 = new File(gameFolder.getAbsolutePath() + Constants.separator + "slot_3");
            String slot0Json, slot1Json, slot2Json, slot3Json;
            if(slot0.exists()){
                slot0Json = FileUtil.readFile(slot0.getAbsolutePath());
                LOGFILE.info("Autosave JSON loaded.");
                retVal.put(0, SaveGameParser.parseGame(slot0Json));
            }
            if(slot1.exists()){
                slot1Json = FileUtil.readFile(slot1.getAbsolutePath());
                LOGFILE.info("Slot 1 JSON loaded.");
                retVal.put(1, SaveGameParser.parseGame(slot1Json));
            }
            if(slot2.exists()) {
                slot2Json = FileUtil.readFile(slot2.getAbsolutePath());
                LOGFILE.info("Slot 2 JSON loaded.");
                retVal.put(2, SaveGameParser.parseGame(slot2Json));
            }
            if(slot3.exists()){
                slot3Json = FileUtil.readFile(slot3.getAbsolutePath());
                LOGFILE.info("Slot 3 JSON loaded.");
                retVal.put(3, SaveGameParser.parseGame(slot3Json));
            }
            return retVal;
        }
        throw new SteamIOException("Unable to find game directory.");
    }

    @Override
    public SaveGame loadSaveGame(int index) throws IOException, SaveFileException {
        HashMap<Integer, SaveGame> saves = loadAllSaveGames();
        if(saves.containsKey(index)){
            return saves.get(index);
        }
        LOGFILE.info("Tried to load save " + String.valueOf(index) + " but no save available.");
        throw new RuntimeException("No save game found at requested slot " + String.valueOf(index) + ".");
    }

    @Override
    public void writeAllSaveGames(HashMap<Integer, SaveGame> saveMap) throws IOException, SaveFileException {
        for(Map.Entry<Integer, SaveGame> saveGameEntry : saveMap.entrySet()){
            switch (saveGameEntry.getKey()){
                case 0:
                    LOGFILE.info("Writing slot_auto");
                    FileUtil.writeStringToFile(gameFolderPath +  Constants.separator + "slot_auto", saveGameEntry.getValue().generateJson());
                    break;
                case 1:
                    LOGFILE.info("Writing slot_1");
                    FileUtil.writeStringToFile(gameFolderPath +  Constants.separator + "slot_1", saveGameEntry.getValue().generateJson());
                    break;
                case 2:
                    LOGFILE.info("Writing slot_2");
                    FileUtil.writeStringToFile(gameFolderPath +  Constants.separator + "slot_2", saveGameEntry.getValue().generateJson());
                    break;
                case 3:
                    LOGFILE.info("Writing slot_3");
                    FileUtil.writeStringToFile(gameFolderPath +  Constants.separator + "slot_3", saveGameEntry.getValue().generateJson());
                    break;
                default:
                    throw new IllegalArgumentException("No slots higher than 3 allowed (Base 0)");
            }
        }
    }

    @Override
    public void backupGameFile() throws IOException {
        SaveFileUtil.mkDirPath(Constants.BACKUP_DIR);
        ZipUtil.zipDir(gameFolderPath, String.format("%sgdt_steam_backup_%s.zip", Constants.BACKUP_DIR, SaveFileUtil.getTimestamp()));
    }

    /*=================================================================================================================
    Static helper functions for locating Steam directory under Windows.
    =================================================================================================================*/

    public static File getDefaultProfilePath(){
        if(OSUtil.detectOperatingSystem() == OSUtil.OperatingSystem.WINDOWS_X64){
            return new File("C:\\Program Files (x86)\\Steam");
        } else if (OSUtil.detectOperatingSystem() == OSUtil.OperatingSystem.WINDOWS){
            return new File("C:\\Program Files\\Steam");
        } else {
            return null;
        }
    }

    public class SteamIOException extends IOException {
        public SteamIOException(){
            super();
        }

        public SteamIOException(String message){
            super(message);
        }
    }
}
