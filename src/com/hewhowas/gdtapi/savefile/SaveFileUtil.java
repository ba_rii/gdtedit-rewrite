package com.hewhowas.gdtapi.savefile;

import com.hewhowas.commons.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 7/09/13
 * Time: 10:27 AM
 * To change this template use File | Settings | File Templates.
 */
public class SaveFileUtil {
    static String getTimestamp(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");
        df.setTimeZone(TimeZone.getDefault());
        return df.format(new Date());
    }

    static void mkDirPath(String backupDir) throws IOException{
        File dir = new File(backupDir);
        if(!dir.exists()){
            dir.mkdirs();
        }
    }

}
