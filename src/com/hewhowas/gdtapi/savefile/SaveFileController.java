package com.hewhowas.gdtapi.savefile;

import com.hewhowas.gdtapi.savegame.SaveGame;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 2/09/13
 * Time: 8:41 PM
 * Interface for classes that load save game data. Allows different implementations
 * for steam and standalone versions.
 */
public interface SaveFileController {
    HashMap<Integer, SaveGame> loadAllSaveGames() throws IOException, SaveFileException;
    SaveGame loadSaveGame(int index) throws IOException, SaveFileException;
    void writeAllSaveGames(HashMap<Integer, SaveGame> saveMap) throws IOException, SaveFileException;
    void backupGameFile() throws IOException;
}
