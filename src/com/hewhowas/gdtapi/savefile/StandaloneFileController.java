package com.hewhowas.gdtapi.savefile;

import com.fasterxml.jackson.core.JsonParseException;
import com.hewhowas.commons.util.FileUtil;
import com.hewhowas.gdtapi.Constants;
import com.hewhowas.gdtapi.savegame.SaveGame;
import com.hewhowas.gdtapi.savegame.SaveGameParser;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 3/09/13
 * Time: 2:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class StandaloneFileController implements SaveFileController {

    private static final String databaseTableName = "ItemTable";
    private File databaseFile;
    private String databaseUri;
    Logger LOGFILE = Logger.getLogger(getClass());

    public StandaloneFileController(File saveDatabaseFile) {
        this.databaseFile = saveDatabaseFile;
        this.databaseUri = "jdbc:sqlite:" + this.databaseFile.getAbsolutePath();
        try {
            Class.forName("org.sqlite.JDBC");
            Connection conn = DriverManager.getConnection("jdbc:sqlite:a.db");
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Override
    public HashMap<Integer, SaveGame> loadAllSaveGames() throws JsonParseException, IOException{
        Connection connection;
        HashMap<Integer, SaveGame> saveGameHashMap = new HashMap<Integer, SaveGame>();
        try {
            connection = DriverManager.getConnection(databaseUri);
        } catch (SQLException e1) {
            LOGFILE.error("Error getting database connection: " + e1.getLocalizedMessage());
            throw new DatabaseException(e1);
        }
        try {
            for(int i = 0; i < 4; i++){
                try{
                    Statement statement = connection.createStatement();
                    String slotPostfix = "";
                    if(i == 0){
                        slotPostfix = "auto";
                    } else {
                        slotPostfix = String.valueOf(i);
                    }
                    String sql = String.format("SELECT * FROM %s WHERE key='slot_%s';", databaseTableName, slotPostfix);
                    LOGFILE.debug("SQL SELECT: " + sql);
                    ResultSet rs = statement.executeQuery(sql);
                    rs.next();
                    String saveGameJson = rs.getString(2);
                    LOGFILE.info("Loaded Savegame JSON: " + saveGameJson);
                    SaveGame tmpSave = SaveGameParser.parseGame(saveGameJson);
                    saveGameHashMap.put(i, tmpSave);
                }
                catch (SQLException e) {
                    LOGFILE.info("No save game available in slot: " + String.valueOf(i));
                }
            }
        }
        finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGFILE.error("Error closing database: " + e.getLocalizedMessage());
                throw new DatabaseException(e);
            }
        }

        return saveGameHashMap;
    }

    @Override
    public SaveGame loadSaveGame(int index) throws IOException, SaveFileException {
        HashMap<Integer, SaveGame> saves = loadAllSaveGames();
        if(saves.containsKey(index)){
            return saves.get(index);
        }
        throw new DatabaseException("No save game found at requested slot " + String.valueOf(index) + ".");
    }

    @Override
    public void writeAllSaveGames(HashMap<Integer, SaveGame> saveMap) throws SaveFileException, IOException {

        for(Map.Entry<Integer, SaveGame> saveGameEntry : saveMap.entrySet()){
            String savePostfix = "";
            if(saveGameEntry.getKey() == 0){
                savePostfix = "auto";
            } else {
                savePostfix = String.valueOf(saveGameEntry.getKey());
            }
            SaveGame saveGame = saveGameEntry.getValue();
            Connection connection;
            try {
                connection = DriverManager.getConnection(databaseUri);
                //Main Save File
                String json = saveGame.generateJson();
                LOGFILE.info("Writing json string to datbase: " + json);
                json = json.replace("'", "''");
                Statement statement = connection.createStatement();
                String sql = String.format("UPDATE %s SET value='%s' WHERE key='slot_%s'", databaseTableName, json, savePostfix);
                LOGFILE.debug("SQL UPDATE: " + sql);
                if(!Constants.KILL_DB_WRITE){
                    statement.executeUpdate(sql);
                } else {
                    LOGFILE.info("Killing database writes.");
                }
            } catch (SQLException e1) {
                LOGFILE.error(e1);
                throw new DatabaseException(e1);
            }
        }
    }

    @Override
    public void backupGameFile() throws IOException{
        SaveFileUtil.mkDirPath(Constants.BACKUP_DIR);
        String tStamp = SaveFileUtil.getTimestamp();
        File newFile = new File(String.format("%s%s_backup_%s", Constants.BACKUP_DIR, databaseFile.getName(), tStamp ));
        FileUtil.copyFile(databaseFile, newFile);
    }

    public class DatabaseException extends RuntimeException {

        private static final long serialVersionUID = -5175092051677682993L;

        public DatabaseException(){
            super();
        }

        public DatabaseException(String message){
            super(message);
        }

        public DatabaseException(Throwable reason){
            super(reason);
        }

    }
}
