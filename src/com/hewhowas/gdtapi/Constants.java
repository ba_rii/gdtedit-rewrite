package com.hewhowas.gdtapi;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 4/09/13
 * Time: 7:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class Constants {
    //String constants
    public static final String separator = System.getProperty("file.separator");
    //Debug constants
    //Prevents database writes from taking place.
    public static final Boolean KILL_DB_WRITE = false;
    public static String BACKUP_DIR;
    public static final Boolean DEBUG_MODE = false;
    static {
        if(DEBUG_MODE){
            BACKUP_DIR = "out" + separator + "GDT_Backup" + separator;
        } else {
            BACKUP_DIR = "Backup" + separator;
        }

    }

}
