package com.hewhowas.gdtapi.savegame;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 3/09/13
 * Time: 3:34 PM
 * Utility class to parse SaveGames.
 */
public class SaveGameParser {

    private static final int STAT_MULTIPLICATION_FACTOR = 500;
    /**
     *
     * @param json The json to create a SaveGame object out of.
     * @return A SaveGame object containing the parsed values.
     * @throws JsonParseException
     * @throws IOException
     */
    public static SaveGame parseGame(String json)throws JsonParseException, IOException {
        SaveGame retVal = new SaveGame(json);
        JsonNode rootNode = new ObjectMapper().readTree(json);
        String saveSlot =  rootNode.path("slot").asText();
        if(saveSlot.equals("autosave")){
            retVal.setSaveSlot(0);
        } else {
            retVal.setSaveSlot(Integer.parseInt(saveSlot));
        }
        //Parse company JSON
        JsonNode companyNode = rootNode.path("company");
        retVal.setCompanyName(companyNode.path("name").asText());
        retVal.setCompanyCash(companyNode.path("cash").asLong());
        retVal.setCompanyFans(companyNode.path("fans").asInt());
        retVal.setCompanyData(companyNode.path("researchPoints").asInt());
        parseStaffFromJson(rootNode, retVal);
        return retVal;
    }


    /**
     *
     * @param originalJson The original JSON that the changes are to be applied to.
     * @param saveGame The SaveGame object to read the new values from.
     * @return A string containing the merged saveGame changes.
     * @throws JsonParseException
     * @throws IOException
     */
    static String generateJson(String originalJson, SaveGame saveGame) throws JsonParseException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.readTree(originalJson);
        ((ObjectNode)rootNode).put("editor", "gdtedit2.0");
        writeCompanyToJson(rootNode, saveGame);
        writeStaffToJson(rootNode, saveGame);
        return mapper.writeValueAsString(rootNode);
    }

    private static void parseStaffFromJson(JsonNode rootNode, SaveGame targetGame){
        LinkedHashMap<String, StaffMember> retVal = new LinkedHashMap<String, StaffMember>();
        JsonNode staffNode = rootNode.path("company").path("staff");
        Iterator<JsonNode> iterator = staffNode.elements();
        int i = 0;
        while(iterator.hasNext()) {
            JsonNode staffMemberNode = iterator.next();
            StaffMember tmpStaffMember = new StaffMember();
            tmpStaffMember.setName(staffMemberNode.path("name").asText());
            double tmpVal;
            tmpVal =  staffMemberNode.path("dF").asDouble() * STAT_MULTIPLICATION_FACTOR;
            tmpStaffMember.setDesignFactor((int)tmpVal);
            tmpVal =  staffMemberNode.path("tF").asDouble() * STAT_MULTIPLICATION_FACTOR;
            tmpStaffMember.setTechnologyFactor((int)tmpVal);
            tmpVal =  staffMemberNode.path("speedF").asDouble() * STAT_MULTIPLICATION_FACTOR;
            tmpStaffMember.setSpeedFactor((int)tmpVal);
            tmpVal =  staffMemberNode.path("qualityF").asDouble() * STAT_MULTIPLICATION_FACTOR;
            tmpStaffMember.setQualityFactor((int)tmpVal);
            tmpVal =  staffMemberNode.path("researchF").asDouble() * STAT_MULTIPLICATION_FACTOR;
            tmpStaffMember.setResearchFactor((int)tmpVal);
            tmpStaffMember.setSalary(staffMemberNode.path("salary").asLong());
            tmpStaffMember.setBugFixingDelta(staffMemberNode.path("bugFixingDelta").asLong());
            tmpStaffMember.setRelaxDelta(staffMemberNode.path("relaxDelta").asLong());
            String sex = staffMemberNode.path("sex").asText();
            if(sex.equals("male")){
                tmpStaffMember.setSex(StaffMember.Sex.MALE);
            } else {
                tmpStaffMember.setSex(StaffMember.Sex.FEMALE);
            }
            tmpStaffMember.setSlot(staffMemberNode.path("slot").asInt());
            tmpStaffMember.setId(staffMemberNode.path("id").asText());
            retVal.put(tmpStaffMember.getId(), tmpStaffMember);
        }
        targetGame.setStaffRoster(retVal);
    }

    private static void writeCompanyToJson(JsonNode rootNode, SaveGame sourceGame){
        JsonNode companyNode = rootNode.path("company");
        ((ObjectNode)companyNode).put("name", sourceGame.getCompanyName());
        ((ObjectNode)companyNode).put("cash", sourceGame.getCompanyCash());
        ((ObjectNode)companyNode).put("fans", sourceGame.getCompanyFans());
        ((ObjectNode)companyNode).put("researchPoints", sourceGame.getCompanyData());
    }

    private static void writeStaffToJson(JsonNode rootNode, SaveGame sourceGame){
        JsonNode staffNode = rootNode.path("company").path("staff");
        Iterator<JsonNode> iterator = staffNode.elements();
        while(iterator.hasNext()) {
            JsonNode staffMemberNode = iterator.next();
            String id = staffMemberNode.path("id").asText();
            if(sourceGame.getStaffRoster().containsKey(id)){
                StaffMember staffMember = sourceGame.getStaffRoster().get(id);
                ((ObjectNode)staffMemberNode).put("name", staffMember.getName());
                ((ObjectNode)staffMemberNode).put("dF", staffMember.getDesignFactor() / STAT_MULTIPLICATION_FACTOR);
                ((ObjectNode)staffMemberNode).put("tF", staffMember.getTechnologyFactor() / STAT_MULTIPLICATION_FACTOR);
                ((ObjectNode)staffMemberNode).put("speedF", staffMember.getSpeedFactor() / STAT_MULTIPLICATION_FACTOR);
                ((ObjectNode)staffMemberNode).put("qualityF", staffMember.getQualityFactor() / STAT_MULTIPLICATION_FACTOR);
                ((ObjectNode)staffMemberNode).put("researchF", staffMember.getResearchFactor() / STAT_MULTIPLICATION_FACTOR);
                ((ObjectNode)staffMemberNode).put("salary", staffMember.getSalary());
                ((ObjectNode)staffMemberNode).put("sex", staffMember.getSex().toString());
                ((ObjectNode)staffMemberNode).put("bugFixingDelta", staffMember.getBugFixingDelta());
                ((ObjectNode)staffMemberNode).put("relaxDelta", staffMember.getRelaxDelta());
            }
        }
    }

}
