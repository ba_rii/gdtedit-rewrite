package com.hewhowas.gdtapi.savegame;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 3/09/13
 * Time: 3:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class StaffMember {

    public enum Sex {
        MALE("male"),
        FEMALE("female");

        String text;

        Sex(String text){
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    private String id;
    private String name;
    private int designFactor;
    private int technologyFactor;
    private int speedFactor;
    private int qualityFactor;
    private int researchFactor;
    private long bugFixingDelta;
    private long relaxDelta;
    private long salary;
    private int slot;
    private Sex sex;

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDesignFactor() {
        return designFactor;
    }

    public void setDesignFactor(int designFactor) {
        this.designFactor = designFactor;
    }

    public double getTechnologyFactor() {
        return technologyFactor;
    }

    public void setTechnologyFactor(int technologyFactor) {
        this.technologyFactor = technologyFactor;
    }

    public double getSpeedFactor() {
        return speedFactor;
    }

    public void setSpeedFactor(int speedFactor) {
        this.speedFactor = speedFactor;
    }

    public double getQualityFactor() {
        return qualityFactor;
    }

    public void setQualityFactor(int qualityFactor) {
        this.qualityFactor = qualityFactor;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public double getResearchFactor() {
        return researchFactor;
    }

    public long getBugFixingDelta() {
        return bugFixingDelta;
    }

    public void setBugFixingDelta(long bugFixingDelta) {
        this.bugFixingDelta = bugFixingDelta;
    }

    public void setResearchFactor(int researchFactor) {
        this.researchFactor = researchFactor;
    }

    public long getRelaxDelta() {
        return relaxDelta;
    }

    public void setRelaxDelta(long relaxDelta) {
        this.relaxDelta = relaxDelta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StaffMember that = (StaffMember) o;

        if (bugFixingDelta != that.bugFixingDelta) return false;
        if (Double.compare(that.designFactor, designFactor) != 0) return false;
        if (Double.compare(that.qualityFactor, qualityFactor) != 0) return false;
        if (relaxDelta != that.relaxDelta) return false;
        if (Double.compare(that.researchFactor, researchFactor) != 0) return false;
        if (salary != that.salary) return false;
        if (slot != that.slot) return false;
        if (Double.compare(that.speedFactor, speedFactor) != 0) return false;
        if (Double.compare(that.technologyFactor, technologyFactor) != 0) return false;
        if (!id.equals(that.id)) return false;
        if (!name.equals(that.name)) return false;
        if (sex != that.sex) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id.hashCode();
        result = 31 * result + name.hashCode();
        temp = Double.doubleToLongBits(designFactor);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(technologyFactor);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(speedFactor);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(qualityFactor);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(researchFactor);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (int) (bugFixingDelta ^ (bugFixingDelta >>> 32));
        result = 31 * result + (int) (relaxDelta ^ (relaxDelta >>> 32));
        result = 31 * result + (int) (salary ^ (salary >>> 32));
        result = 31 * result + slot;
        result = 31 * result + sex.hashCode();
        return result;
    }
}
