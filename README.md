GDTEdit is a save game editor for Game Dev Tycoon. It currently supports both Steam and Standalone installs, and SOME
effort has gone into making it semi-maintainable. There are 3 main classes or implementations:

com.hewhowas.gdtapi.savefile.SaveFileController is the interface that must be implemented by any "Controllers". These
are responsible for opening the save file, and reading the JSON in. Concrete implementations exist under
SteamFileController and StandaloneFileController.

com.hewhowas.gdtapi.savegame.SaveGameParser is a static util class used to convert JSON to a SaveGame object and vice versa.

com.hewhowas.gdtapi.SaveGame & StaffMember are data classes.

Other than that there are supporting exceptions, util classes and a few custom controls.
